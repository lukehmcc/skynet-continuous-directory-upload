package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"strings"
	"time"

	skynet "github.com/NebulousLabs/go-skynet"
)

// uploads files from a directory and adds each
// skylink to a file, then deletes each one

// create a client
var client = skynet.New()

func main() {
	// just keep loopin the script forever
	for true {
		uploadDaThings()
		time.Sleep(10 * time.Second)
	}
}

func uploadDaThings() {
	// First grabs the config
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
		return
	}
	file, err := os.Open(usr.HomeDir + "/.config/skynet-continuous-directory-upload/continuous-upload.conf")
	var configs []string
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		configs = append(configs, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Println(configs)
	// walks and grabs all of the file names
	var fileNames []string
	err = filepath.Walk(configs[1],
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if len(path) > 3 && path[:4] == ".git" || path == "." || isDir(path) {
				// does nothing
			} else {
				fileNames = append(fileNames, path)
				println(path)
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
	// fmt.Println(fileNamesPre)
	// It then uses http post to upload all of the files one by one
	// I know that doing it non-async-ly isn't the best method, but it's
	// the only easy way to do it without having to introduce a database
	// and still have the ability to have a consise list of files
	for i := 0; i < len(fileNames); i++ {
		// upload
		skylink, err := client.UploadFile(fileNames[i], skynet.DefaultUploadOptions)
		if err != nil {
			panic("Unable to upload: " + err.Error())
		}
		// cuts to the pure file name
		locationTemp := fileNames[i]
		if strings.Contains(fileNames[i], "/") {
			locationTemp = fileNames[i][strings.LastIndex(fileNames[i], "/")+1:]
		}
		fmt.Println("to be printed", locationTemp+" -> "+skylink)
		fmt.Println("location to log to", usr.HomeDir+"/.config/skynet-continuous-directory-upload/uploaded.log")
		appendToFile(locationTemp+" -> "+skylink, usr.HomeDir+"/.config/skynet-continuous-directory-upload/uploaded.log")
	}
	err = RemoveContents(configs[1])
	if err != nil {
		fmt.Println("deleting folder didn't go so well captain")
	}
}

// appends skylinks and names to file
func appendToFile(text string, location string) {
	f, err := os.OpenFile(location, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	// writes thing
	if _, err := f.WriteString(text + "\n"); err != nil {
		log.Println(err)
	}
}

// checks if something is a file or dir
func isDir(path string) bool {
	fi, err := os.Stat(path)
	if err != nil {
		fmt.Println(err)
		return true
	}
	switch mode := fi.Mode(); {
	case mode.IsDir():
		return true
	case mode.IsRegular():
		return false
	}
	// just in case something isn't a file or dir?
	return true
}

// delete all(stolen from the stack ofc)
func RemoveContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}
