# checks if user argument is passed
if [ "$#" -ne 1 ]
then
    # if no user is passed then say it should be
    echo "make sure to pass a username when running this script, e.g. `sudo sh setup.sh lukehmcc` if your username is lukehmcc" 
else
    # downloads the exe and config file
    sudo mkdir -p /home/$1/.config/skynet-continuous-directory-upload/
    sudo chmod 644 /home/$1/.config/skynet-continuous-directory-upload/ #fixes perms more
    sudo curl https://gitlab.com/lukehmcc/skynet-continuous-directory-upload/-/raw/master/dist/linux/continuous-upload -o /home/$1/.config/skynet-continuous-directory-upload/continuous-upload 
    sudo curl https://gitlab.com/lukehmcc/skynet-continuous-directory-upload/-/raw/master/configs/continuous-upload.conf -o /home/$1/.config/skynet-continuous-directory-upload/continuous-upload.conf 
    #fix perms
    sudo chmod a+r /home/user/.config/skynet-continuous-directory-upload/continuous-upload.conf
    sudo chmod a+x /home/user/.config/skynet-continuous-directory-upload/continuous-upload
    #overwrite the config because it's being annoying
    sudo printf "https://siasky.net\n/home/$1/constantUpload" > /home/$1/.config/skynet-continuous-directory-upload/continuous-upload.conf

    # makes service
    sudo printf "[Unit]\nDescription=Continuous Upload to Skynet\n\nWants=network.target\nAfter=syslog.target network-online.target\n\n[Service]\nType=simple\nExecStart=/home/$1/.config/skynet-continuous-directory-upload/continuous-upload\nRestart=on-failure\nRestartSec=10\nKillMode=process\nUser=$1\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/continuous-upload.service
    sudo systemctl daemon-reload
    sudo systemctl start continuous-upload
    sudo systemctl restart continuous-upload
    sudo systemctl status continuous-upload
fi