# skynet-continuous-directory-upload

It's a basic tool that continuously uploads all the contents of a folder to a specified portal

Setup:
It's as simple as downloading the setup script that you want to use, currently I only have this built out for debian based systems. To do this:
```
curl https://gitlab.com/lukehmcc/skynet-continuous-directory-upload/-/raw/master/setupScripts/setup.sh -o setup.sh
sudo sh setup.sh userNameYouWantToInstallToHere
```
If it's broken feel free to message me on discord at ℂ𝕠𝕧𝕒𝕝𝕖𝕟𝕥@5766 (:
(or just build it yourself and designate it a service, it's really not that hard)

To customize futher, go to `~/.config/skynet-continuous-directory-upload/continuous-upload.conf` where you'll find this config file: 
```
https://siasky.net
/home/$USER/constantUpload
```
The first entry is supposed to control the portal, but I don't think the skd has portal options soooo
The second entry controls what directory to watch and upload from
    Also please make sure to have the directory to upload to has read-write permissions allowed so the script can have the ability to delete files from it. If it doesn't it'll repeatedly keep re-uploading the same files over and over again :)

Oh also, the upload log(for a list of uploaded files) is `~/.config/skynet-continuous-directory-upload/uploaded.log`